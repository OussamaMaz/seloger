export enum TypeImmobilier {
  Maison = 'maison',
  Appartement = 'appartement',
  Local = 'local',
  Terrain = 'terrain',
}
